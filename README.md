### [qrpc](https://gitee.com/qiudx/qrpc)(分布式RPC框架)
- qrpc是一个轻量级分布式RPC框架,可以和Spring框架无缝集成。
- qrpc基于TCP协议,使用Netty通信
- 序列化方式使用google开源的Protobuf
- 客服端实现了同步和异步两种调用方式,实现了注册中心(zookeeper),实现了服务监控,有良好的配置的扩展性

**在项目组的性能测试(每季度的考核)中性能优越,整体表现很好**