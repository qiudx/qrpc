package com.qrpc.admin.controller;

import com.google.common.base.Splitter;
import com.qrpc.admin.mode.ServiceModel;
import com.qrpc.admin.mode.ServiceProvider;
import com.qrpc.common.enums.ServerStatusEnum;
import org.apache.curator.framework.CuratorFramework;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/admin")
@Controller
public class AdminController {
    @Value("${ZK_PATH_PREFIX}")
    private String ZK_PATH_PREFIX;

    private final CuratorFramework curatorFramework;

    @Autowired
    public AdminController(CuratorFramework curatorFramework) {
        this.curatorFramework = curatorFramework;
    }


    @RequestMapping("/index")
    public String index(Model model) throws Exception {
        List<String> services = curatorFramework.getChildren().forPath(ZK_PATH_PREFIX);
        final List<ServiceModel> serviceModels = new ArrayList<>();
        if (!CollectionUtils.isEmpty(services)) {
            for (String serviceName : services) {
                ServiceModel serviceModel = new ServiceModel();
                serviceModel.setServiceName(serviceName);
                List<ServiceProvider> serviceProviders = new ArrayList<>();
                List<String> serverPayLoadList = curatorFramework.getChildren().forPath(ZK_PATH_PREFIX + "/" + serviceName);
                if (!CollectionUtils.isEmpty(serverPayLoadList)) {
                    for (String serverPayLoad : serverPayLoadList) {
                        String node = new String(curatorFramework.getData().forPath(ZK_PATH_PREFIX + "/" + serviceName + "/" + serverPayLoad));
                        ServiceProvider serviceProvider = new ServiceProvider();
                        List<String> serviceProviderPayLoadTokens = Splitter.on(":").splitToList(node);
                        serviceProvider.setIp(serviceProviderPayLoadTokens.get(0));
                        serviceProvider.setPort(serviceProviderPayLoadTokens.get(1));
                        serviceProvider.setManipulate("");
                        serviceProvider.setStatus(ServerStatusEnum.AVAILABLE.getName());
                        serviceProviders.add(serviceProvider);
                    }
                }
                serviceModel.setServiceProviders(serviceProviders);
                serviceModels.add(serviceModel);
            }
        }
        model.addAttribute("services", serviceModels);
        return "index";
    }

    @RequestMapping("/remove")
    public String remove(Model model, String ip) throws Exception {



        List<String> services = curatorFramework.getChildren().forPath(ZK_PATH_PREFIX);
        final List<ServiceModel> serviceModels = new ArrayList<>();
        if (!CollectionUtils.isEmpty(services)) {
            for (String serviceName : services) {
                ServiceModel serviceModel = new ServiceModel();
                serviceModel.setServiceName(serviceName);
                List<ServiceProvider> serviceProviders = new ArrayList<>();
                List<String> serverPayLoadList = curatorFramework.getChildren().forPath(ZK_PATH_PREFIX + "/" + serviceName);
                if (!CollectionUtils.isEmpty(serverPayLoadList)) {
                    for (String serverPayLoad : serverPayLoadList) {
                        String node = new String(curatorFramework.getData().forPath(ZK_PATH_PREFIX + "/" + serviceName + "/" + serverPayLoad));
                        ServiceProvider serviceProvider = new ServiceProvider();
                        List<String> serviceProviderPayLoadTokens = Splitter.on(":").splitToList(node);
                        if (serviceProviderPayLoadTokens.get(0).equals(ip)) {
                            serviceProvider.setIp(serviceProviderPayLoadTokens.get(0));
                            serviceProvider.setPort(serviceProviderPayLoadTokens.get(1));
                            serviceProvider.setManipulate("");
                            serviceProviders.add(serviceProvider);
                        } else {
                            serviceProvider.setIp(serviceProviderPayLoadTokens.get(0));
                            serviceProvider.setPort(serviceProviderPayLoadTokens.get(1));
                            serviceProvider.setManipulate("");
                            serviceProvider.setStatus(ServerStatusEnum.AVAILABLE.getName());
                            serviceProviders.add(serviceProvider);
                        }
                    }
                }
                serviceModel.setServiceProviders(serviceProviders);
                serviceModels.add(serviceModel);
            }
        }
        model.addAttribute("services", serviceModels);
        return "index";
    }
}
