package com.qrpc.admin.mode;

import lombok.Data;

import java.util.List;

@Data
public class ServiceModel {
	private String serviceName;
	private String startTime;
	private List<ServiceProvider> serviceProviders;
}
