package com.qrpc.admin.mode;

import lombok.Data;

@Data
public class ServiceProvider {
    private String ip;
    private String port;
    private String status;
    private String manipulate;
}
