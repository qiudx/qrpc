<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Theme Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="/static/css/bootstrap.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="/static/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="/static/css/theme.css" rel="stylesheet">
    <script type="text/javascript" src="../static/js/jquery.js"></script>
    <script type="text/javascript">
        function remove(a) {
            alert(a);
            $.ajax({
                url: "/admin/remove",
                data: {"ip": ""},
                type: "post",
                dataType: "json",
                success: function () {

                }
            })
        }

        function disable(a) {

        }

        //        $(function () {
        //            $('#button').click(function () {
        //
        //            })
        //        });
    </script>
</head>

<body>

<a href="${sctx}jquery-1.4.2.min.js">全局属性</a>

<div class="container theme-showcase" role="main">

    <div class="page-header">
        <h1>Services</h1>
    </div>
<#list services as service>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">${service.serviceName}</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>IP</th>
                            <th>Port</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            <#list service.serviceProviders as serviceProvider>
                            <tr>
                                <td>${serviceProvider.ip}</td>
                                <td>${serviceProvider.port}</td>
                                <td>${serviceProvider.status}</td>
                            </tr>
                            </#list>
                    </table>
                </div>
            </div>
        </div>
    </div>
</#list>


</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/static/js/jquery.js"></script>
<script src="/static/js/docs.min.js"></script>
<script src="/static/js/bootstrap.js"></script>
</body>
</html>
