package com.qrpc.server.qrpc;

import com.qrpc.common.model.QrpcRequest;
import com.qrpc.common.model.QrpcResponse;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

import static com.qrpc.server.zk.impl.ZkDiscovery.nioConnectMap;

@Component
@Slf4j
@ChannelHandler.Sharable
public class QrpcClient extends SimpleChannelInboundHandler<QrpcResponse> {

    private Map<Long, Future> futureMap = new ConcurrentHashMap<>();


    Future send(QrpcRequest request, String serviceAddress) throws Exception {
        Future future = new Future();
        ChannelFuture channelFuture = nioConnectMap.get(serviceAddress);
        futureMap.put(request.getRequestId(), future);
        channelFuture.channel().writeAndFlush(request);
        return future;
    }

    @Override
    protected void messageReceived(ChannelHandlerContext channelHandlerContext, QrpcResponse qrpcResponse) {
        Future future = futureMap.remove(qrpcResponse.getRequestId());
        if (future != null) {
            try {
                future.responseQueue.put(qrpcResponse);
            } catch (InterruptedException e) {
                log.error("netty数据获取失败  {}", qrpcResponse.getRequestId());
            }
        }
    }


    Future asyncSend(QrpcRequest request, String serviceAddress) {
        ChannelFuture channelFuture = nioConnectMap.get(serviceAddress);
        Future future = new Future();
        futureMap.put(request.getRequestId(), future);
        channelFuture.channel().writeAndFlush(request);
        return future;
    }

    public class Future {
        public ArrayBlockingQueue<QrpcResponse> responseQueue = new ArrayBlockingQueue<>(1);
    }



}
