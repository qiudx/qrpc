package com.qrpc.server.qrpc;


import com.qrpc.common.annotation.Qasync;
import com.qrpc.common.model.QrpcRequest;
import com.qrpc.common.registry.ServiceDiscovery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Proxy;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

@Component
@Slf4j
public class QrpcProxy {


    private String serviceAddress;

    private ServiceDiscovery serviceDiscovery;
    private final QrpcClient client;
    private static AtomicLong atomicLong = new AtomicLong();


    @Autowired
    public QrpcProxy(ServiceDiscovery serviceDiscovery, QrpcClient client) {
        this.serviceDiscovery = serviceDiscovery;
        this.client = client;
    }


    public <T> T create(final Class<?> interfaceClass) {
        // 创建动态代理对象
        Object instance = Proxy.newProxyInstance(
                interfaceClass.getClassLoader(),
                new Class<?>[]{interfaceClass},
                (proxy, method, args) -> {
                    Qasync methodAnnotation = method.getAnnotation(Qasync.class);
                    QrpcRequest request = new QrpcRequest();
                    request.setRequestId(atomicLong.incrementAndGet());
                    request.setInterfaceName(method.getDeclaringClass().getName());
                    request.setMethodName(method.getName());
                    request.setParameterTypes(method.getParameterTypes());
                    request.setParameters(args);
                    if (serviceDiscovery != null) {
                        String serviceName = interfaceClass.getName();
                        serviceAddress = serviceDiscovery.discover(serviceName);
                        log.debug("discover service: {} => {}", serviceName, serviceAddress);
                    }
                    if (methodAnnotation == null) {
                        QrpcClient.Future future = client.send(request, serviceAddress);
                        return future.responseQueue.poll(4, TimeUnit.SECONDS).getResult();
                    }
                    CompletableFuture<QrpcClient.Future> future = CompletableFuture.supplyAsync(() -> client.asyncSend(request, serviceAddress));
                    return future.get();
                }
        );
        return (T) instance;
    }
}
