package com.qrpc.server.utils;

import com.qrpc.server.qrpc.QrpcProxy;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;

public class ProxyFactoryBean<T> implements FactoryBean<T> {
    private Class<T> proxyInterface;
    @Autowired
    private QrpcProxy qrpcProxy;

    public ProxyFactoryBean() {
    }

    public ProxyFactoryBean(Class<T> proxyInterface) {
        this.proxyInterface = proxyInterface;
    }

    @Override
    public T getObject() {
        return qrpcProxy.create(proxyInterface);
    }

    @Override
    public Class<?> getObjectType() {
        return this.proxyInterface;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}

