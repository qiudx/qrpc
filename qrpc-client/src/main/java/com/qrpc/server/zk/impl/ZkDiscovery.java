package com.qrpc.server.zk.impl;

import com.qrpc.common.model.QrpcRequest;
import com.qrpc.common.model.QrpcResponse;
import com.qrpc.common.utils.codec.QrpcDecoder;
import com.qrpc.common.utils.codec.QrpcEncoder;
import com.qrpc.server.qrpc.QrpcClient;
import com.qrpc.common.registry.ServiceDiscovery;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import static com.qrpc.common.constant.Constant.ZK_PATH_PREFIX;


@Component
@Slf4j
public class ZkDiscovery implements ServiceDiscovery, InitializingBean {

    @Value("${zkAddress}")
    private String zkAddress;
    private CuratorFramework zkClient;
    public static Map<String, ChannelFuture> nioConnectMap = new HashMap<>();
    private final QrpcClient qrpcClient;
    private Map<String, List<String>> hashMap = new HashMap<>();

    @Autowired
    public ZkDiscovery(QrpcClient qrpcClient) {
        this.qrpcClient = qrpcClient;
    }

    @Override
    public String discover(String name) {
        // 获取 service 节点
        String servicePath = ZK_PATH_PREFIX + "/" + name;
        List<String> nodeList = this.hashMap.get(servicePath);
        if (nodeList.isEmpty()) {
            try {
                getZkNodeData();
                if (nodeList.isEmpty()) {
                    throw new RuntimeException(String.format("服务不存在: %s", servicePath));
                }
                nodeList = this.hashMap.get(servicePath);
            } catch (Exception e) {
                log.error("zk服务获取失败{}", servicePath);
            }
        }
        if (nodeList.size() > 1) {
            int nextInt = ThreadLocalRandom.current().nextInt(nodeList.size());
            String address = nodeList.get(nextInt);
            log.info("获取随机节点: {}", address);
            return address;
        } else {
            String address = nodeList.get(0);
            log.info("获取节点: {}", address);
            return address;
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        zkClient = CuratorFrameworkFactory.builder().connectString(zkAddress)
                .retryPolicy(new ExponentialBackoffRetry(1000, 3)).build();
        zkClient.start();
        log.info(" zk客服端连接成功");
        getZkNodeData();
        TreeCache treeCache = new TreeCache(zkClient, ZK_PATH_PREFIX);
        treeCache.getListenable().addListener((client, event) -> {
            ChildData data = event.getData();
            if (data != null) {
                switch (event.getType()) {
                    case NODE_ADDED:
                        log.info("新增服务成功{}", data.getPath());
                        getZkNodeData();
                        break;
                    case NODE_REMOVED:
                        log.info("{}服务已断开", data.getPath());
                        getZkNodeData();
                        break;
                    default:
                        break;
                }
            }
        });
        treeCache.start();
    }

    private synchronized void getZkNodeData() throws Exception {
        hashMap.clear();
        String servicePath = ZK_PATH_PREFIX;
        if (zkClient.checkExists().forPath(servicePath) == null) {
            throw new RuntimeException(String.format("zk根路径节点不存在: %s", servicePath));
        }
        //服务列表
        List<String> addressList = zkClient.getChildren().forPath(servicePath);
        addressList.forEach(a -> {
            List<String> list = new ArrayList<>();
            try {
                String serverPath = servicePath + "/" + a;
                //节点列表
                List<String> strings = zkClient.getChildren().forPath(serverPath);
                strings.forEach(b -> {
                    try {
                        String path = serverPath + "/" + b;
                        byte[] bytes = zkClient.getData().forPath(path);
                        String s = new String(bytes);
                        list.add(s);
                    } catch (Exception e) {
                        log.error("zk服务节点获取失败{}", zkAddress + a + b);
                    }
                });
                hashMap.put(serverPath, list);
            } catch (Exception e) {
                log.error("zk服务获取失败{}", zkAddress + a);
            }
        });
        initChannalFuture();
    }

    private  void initChannalFuture() throws Exception {
        nioConnectMap.clear();
        Map<String, List<String>> map = hashMap;
        Optional<String> first = map.keySet().stream().findFirst();
        if (first.isPresent()) {
            String s = first.get();
            List<String> list = map.get(s);
            list.forEach(a -> {
                String[] strings = a.split(":");
                EventLoopGroup group = new NioEventLoopGroup(32);
                Bootstrap bootstrap = new Bootstrap();
                ChannelFuture future = null;
                try {
                    future = bootstrap.group(group).channel(NioSocketChannel.class)
                            .option(ChannelOption.SO_KEEPALIVE, true)
                            .option(ChannelOption.TCP_NODELAY, true)
                            .handler(new ChannelInitializer<Channel>() {
                                @Override
                                protected void initChannel(Channel channel) throws Exception {
                                    ChannelPipeline pipeline = channel.pipeline();
                                    pipeline.addLast(new QrpcEncoder(QrpcRequest.class))
                                            .addLast(new LengthFieldBasedFrameDecoder(65536, 0, 4, 0, 0))
                                            .addLast(new QrpcDecoder(QrpcResponse.class))
                                            .addLast(qrpcClient);
                                }
                            })
                            .connect(strings[0], Integer.parseInt(strings[1])).sync();
                } catch (InterruptedException e) {
                    log.error("nio  连接创建失败");
                }
                nioConnectMap.put(a, future);
            });
        }
    }
}