package com.qrpc.common.enums;


public enum ServerStatusEnum {

    AVAILABLE("available", "正常"),
    DISABLED("disabled", "禁用");
    String name;
    String code;

    ServerStatusEnum(String code, String name) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
