package com.qrpc.common.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class QrpcRequest {
    private long requestId;
    private String interfaceName;
    private String methodName;
    private Class<?>[] parameterTypes;
    private Object[] parameters;
}
