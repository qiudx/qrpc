package com.qrpc.common.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class QrpcResponse {

    private long requestId;
    private Object result;
}
