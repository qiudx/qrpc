package com.qrpc.common.registry;

public interface ServiceDiscovery {

    String discover(String serviceName);
}