package com.qrpc.common.registry;

public interface ServiceRegistry {
    void register(String serviceName, String serviceAddress);
}
