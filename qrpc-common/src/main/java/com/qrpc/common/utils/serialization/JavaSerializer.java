/**
 * Zentech-Inc.com
 * Copyright (C) 2017 All Rights Reserved.
 */
package com.qrpc.common.utils.serialization;



import java.io.*;

/**
 * @author qiudx
 * @version $Id JavaSerializer.java, v 0.1 2017-09-22 12:35 qiudx Exp $$
 */
public class JavaSerializer {

    /**
     * 克隆
     */
    public static Object clone(Object object) {
        return deserialize(serialize(object));
    }

    /**
     * 序列化
     *
     * @param obj
     * @param outputStream
     */
    public static void serialize(Object obj, OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("The OutputStream must not be null");
        }
        try (ObjectOutputStream out = new ObjectOutputStream(outputStream)) {
            out.writeObject(obj);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 序列化
     *
     * @param obj
     * @return
     */
    public static byte[] serialize(Object obj) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(512);
        serialize(obj, baos);
        return baos.toByteArray();
    }

    /**
     * 反序列化
     *
     * @param inputStream
     * @return
     */
    public static Object deserialize(InputStream inputStream) {
        if (inputStream == null) {
            throw new IllegalArgumentException("The InputStream must not be null");
        }
        ObjectInputStream in = null;
        try {
            // stream closed in the finally
            in = new ObjectInputStream(inputStream);
            return in.readObject();
        } catch (ClassNotFoundException | IOException ex) {
            return null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
            }
        }
    }

    /**
     * 反序列化
     */
    public static Object deserialize(byte[] objectData) {
        if (objectData == null) {
            throw new IllegalArgumentException("The byte[] must not be null");
        }
        ByteArrayInputStream bais = new ByteArrayInputStream(objectData);
        return deserialize(bais);
    }
}
