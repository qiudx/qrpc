package com.qrpc.server.qrpc;

import com.qrpc.common.model.QrpcRequest;
import com.qrpc.common.model.QrpcResponse;
import com.qrpc.common.utils.codec.QrpcDecoder;
import com.qrpc.common.utils.codec.QrpcEncoder;
import com.qrpc.server.utils.IpUtils;
import com.qrpc.server.zk.impl.ZkRegistry;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.util.concurrent.DefaultExecutorServiceFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.channels.spi.SelectorProvider;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class QrpcServer implements ApplicationContextAware {
    private String servicePort;
    @Autowired
    private ZkRegistry zkRegistry;
    private ConcurrentHashMap<String, Object> map = new ConcurrentHashMap<>();
    private static ApplicationContext applicationContext = null;

    public QrpcServer(String servicePort, ZkRegistry zkRegistry) {
        this.servicePort = servicePort;
        this.zkRegistry = zkRegistry;
    }


    public void init() throws InterruptedException, SocketException {

        ApplicationContext context = applicationContext;
        Map<String, Object> beans = context.getBeansWithAnnotation(Service.class);
        beans.forEach((k, v) -> {
            Service name = v.getClass().getAnnotation(Service.class);
            map.put(name.value(), v);
        });
        EventLoopGroup boss = new NioEventLoopGroup();
//        EventLoopGroup boss = new EpollEventLoopGroup();
//        EventLoopGroup worker = new EpollEventLoopGroup(512);
        EventLoopGroup worker = new NioEventLoopGroup(1024, new DefaultExecutorServiceFactory("qrpc-server-worker"), SelectorProvider.provider());
        String ip = IpUtils.getRealHost();
        String servicepath = ip + ":" + servicePort;
        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            ChannelFuture future = bootstrap.group(boss, worker).channel(NioServerSocketChannel.class).childHandler(new ChannelInitializer<Channel>() {
                @Override
                protected void initChannel(Channel channel) throws Exception {
                    channel.pipeline()
                            .addLast(new LengthFieldBasedFrameDecoder(65536, 0, 4, 0, 0))
                            .addLast(new QrpcDecoder(QrpcRequest.class))
                            .addLast(new QrpcEncoder(QrpcResponse.class))
                            .addLast(new QrpcServerHandler(map));

                }
            })
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    .option(ChannelOption.TCP_NODELAY, true)
                    .bind(new InetSocketAddress(Integer.parseInt(servicePort))).sync();
            // 注册 RPC 服务地址
            if (zkRegistry != null) {
                for (String interfaceName : map.keySet()) {
                    zkRegistry.register(interfaceName, servicepath);
                    log.info("注册服务: {} => {}", interfaceName, servicepath);
                }
            }
            log.info("服务端口 {}", servicepath);
            future.channel().closeFuture().sync();
        } finally {
            worker.shutdownGracefully();
            boss.shutdownGracefully();
        }


    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        QrpcServer.applicationContext = applicationContext;
    }
}
