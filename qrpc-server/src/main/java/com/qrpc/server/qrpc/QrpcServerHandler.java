package com.qrpc.server.qrpc;

import com.qrpc.common.model.QrpcRequest;
import com.qrpc.common.model.QrpcResponse;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cglib.reflect.FastClass;
import org.springframework.cglib.reflect.FastMethod;

import java.util.Map;
import java.util.concurrent.*;

@Slf4j
public class QrpcServerHandler extends SimpleChannelInboundHandler<QrpcRequest> {


    private final Map<String, Object> handlerMap;

    QrpcServerHandler(Map<String, Object> handlerMap) {
        this.handlerMap = handlerMap;
    }

    private Map<Class<?>, FastClass> map = new ConcurrentHashMap<>();

    private Object handle(QrpcRequest request) throws Exception {
        String serviceName = request.getInterfaceName();
        Object serviceBean = handlerMap.get(serviceName);
        if (serviceBean == null) {
            throw new RuntimeException(String.format("该服务未注册: %s", serviceName));
        }
        Class<?> serviceClass = serviceBean.getClass();
        FastClass fastClass = map.computeIfAbsent(serviceClass, k -> FastClass.create(serviceClass));
        String methodName = request.getMethodName();
        Class<?>[] parameterTypes = request.getParameterTypes();
        Object[] parameters = request.getParameters();
        FastMethod serviceFastMethod = fastClass.getMethod(methodName, parameterTypes);
        return serviceFastMethod.invoke(serviceBean, parameters);
    }


    @Override
    protected void messageReceived(ChannelHandlerContext ctx, QrpcRequest request) throws Exception {

        CompletableFuture.runAsync(() -> {
            QrpcResponse response = new QrpcResponse();
            response.setRequestId(request.getRequestId());
            try {
                Object result = handle(request);
                response.setResult(result);
            } catch (Exception e) {
                log.error("handler调用失败", e);
            }
            ctx.writeAndFlush(response);
        });
    }

}
