package com.qrpc.server.zk.impl;

import com.qrpc.common.registry.ServiceRegistry;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;

import static com.qrpc.common.constant.Constant.ZK_PATH_PREFIX;

@Slf4j
public class ZkRegistry implements ServiceRegistry {


    private CuratorFramework curatorFramework;


    private ZkRegistry(String zkAddress) {
        curatorFramework = CuratorFrameworkFactory.builder().connectString(zkAddress)
                .retryPolicy(new ExponentialBackoffRetry(1000, 2))
                .connectionTimeoutMs(2000)
                .sessionTimeoutMs(1000)
                .build();
        curatorFramework.start();
        log.info("connect zk");
    }

    @Override
    public void register(String serviceName, String serviceAddress) {
        String registryPath = ZK_PATH_PREFIX;
        try {
            if (curatorFramework.checkExists().forPath(registryPath) == null) {
                curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT).forPath(registryPath);
                log.info("创建zk根路径: {}", registryPath);
            }
        } catch (Exception e) {
            log.error("创建zk根路径失败: {}", registryPath);
        }
        String servicePath = registryPath + "/" + serviceName;
        try {
            if (curatorFramework.checkExists().forPath(registryPath) == null) {
                curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT).forPath(servicePath);
                log.info("创建服务节点: {}", servicePath);
            }
        } catch (Exception e) {
            log.error("创建服务节点失败: {}", servicePath);
        }
        // 创建 address 节点（临时）
        String addressPath = servicePath + "/node-";
        try {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL_SEQUENTIAL).forPath(addressPath, serviceAddress.getBytes());
        } catch (Exception e) {
            log.error("创建服务节点失败: {}", servicePath);
        }
    }
}