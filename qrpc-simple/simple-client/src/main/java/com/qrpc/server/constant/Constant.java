package com.qrpc.server.constant;

public interface Constant {


    String BASE_GETSTRING = "/getstring";
    String BASE_GETSPERSON = "/getpersion";
    String BASE_GETUSER = "/getuser";
    String BASE_GETPERSONBYNAME = "/getpersonbyname";
}
