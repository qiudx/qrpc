package com.qrpc.server.controller;

import com.qrpc.api.facade.TestService;
import com.qrpc.api.vo.Person;
import com.qrpc.api.vo.User;
import com.qrpc.common.model.QrpcResponse;
import com.qrpc.server.qrpc.QrpcClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

import static com.qrpc.server.constant.Constant.BASE_GETPERSONBYNAME;
import static com.qrpc.server.constant.Constant.BASE_GETSPERSON;
import static com.qrpc.server.constant.Constant.BASE_GETSTRING;
import static com.qrpc.server.constant.Constant.BASE_GETUSER;


/**
 * @author qiudx
 */
@RestController
public class TestController {
    @Autowired
    private TestService testService;


    @GetMapping(BASE_GETSTRING)
    public String getString() {
        return testService.getString();
    }

    @GetMapping(BASE_GETSPERSON)
    public Person getPerson(String name) {
        return testService.getPerson(name);
    }


    @GetMapping(BASE_GETUSER)
    public User getUser(String id) {
        return testService.getUser(id);
    }

    @GetMapping(BASE_GETPERSONBYNAME)
    public Person getPersonPerson(String name) throws InterruptedException {
        QrpcClient.Future future = (QrpcClient.Future) testService.getPersonByName(name);
        QrpcResponse qrpcResponse = future.responseQueue.poll(2, TimeUnit.SECONDS);
        return (Person) qrpcResponse.getResult();
    }
}
