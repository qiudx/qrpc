package test.java;

import com.qrpc.api.facade.TestService;
import com.qrpc.api.vo.Person;
import com.qrpc.common.model.QrpcRequest;
import com.qrpc.server.ClientApplication;
import com.qrpc.server.qrpc.QrpcClient;
import com.qrpc.server.qrpc.QrpcProxy;
import com.qrpc.server.utils.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@SpringBootTest(classes = ClientApplication.class)
@RunWith(SpringRunner.class)
@Slf4j
public class HttpTest {

    private TestService helloService;

    @Autowired
    private QrpcClient client;

    @org.junit.Test
    public void getPerson() {
        int loopCount = 100000;

        if (helloService == null) {
            QrpcProxy qrpcProxy = SpringContextHolder.getBean(QrpcProxy.class);
            helloService = qrpcProxy.create(TestService.class);
        }
        long start = System.currentTimeMillis();
        for (int i = 0; i < loopCount; i++) {
            helloService.getPerson("a");
        }
        long time = System.currentTimeMillis() - start;
        System.out.println("loop: " + loopCount);
        System.out.println("time: " + time + "ms");
        System.out.println("tps: " + loopCount * 1000 / time);
        System.exit(0);
    }

    @org.junit.Test
    public void setHelloService() throws Exception {
        int loopCount = 1000000;
        long start = System.currentTimeMillis();
//        System.out.println(start);
//        long s;
        for (int i = 0; i < loopCount; i++) {
//            s = System.currentTimeMillis();
            QrpcRequest request = new QrpcRequest();
            request.setRequestId(1L);
            request.setInterfaceName("TestService");
            request.setMethodName("getString");
            request.setParameterTypes(new Class[]{});
            request.setParameters(new Object[]{});
//            client.send(request, "10.0.3.185:8000");
//            System.out.println(i + " : " + (System.currentTimeMillis() - s) + "  -  " + response.getResult());
        }
        long time = System.currentTimeMillis() - start;
        System.out.println("loop: " + loopCount);
        System.out.println("time: " + time + "ms");
        System.out.println("tps: " + loopCount * 1000 / time);
    }

    //    public void main() {
//
//        RpcProxy rpcProxy = context.getBean(RpcProxy.class);
//
//        int loopCount = 100;
//
//        long start = System.currentTimeMillis();
//
//        HelloService helloService = rpcProxy.create(HelloService.class);
//        for (int i = 0; i < loopCount; i++) {
//            String result = helloService.hello("World");
//            System.out.println(result);
//        }
//
//        long time = System.currentTimeMillis() - start;
//        System.out.println("loop: " + loopCount);
//        System.out.println("time: " + time + "ms");
//        System.out.println("tps: " + (double) loopCount / ((double) time / 1000));
//
//        System.exit(0);
//    }
    @Test
    public void test() {

        long nanos = TimeUnit.SECONDS.toNanos(3);
//        long nanoTime1 = System.nanoTime();
        long nanoTime2 = System.nanoTime();
        long l = System.currentTimeMillis();
        System.out.println(l);
        System.out.println(nanos);
        System.out.println(nanoTime2);
    }

    @Test
    public void asyncTest() {
        int loopCount = 1;

        if (helloService == null) {
            QrpcProxy qrpcProxy = SpringContextHolder.getBean(QrpcProxy.class);
            helloService = qrpcProxy.create(TestService.class);
        }
        long start = System.currentTimeMillis();
        for (int i = 0; i < loopCount; i++) {
            Person person = (Person) helloService.getPersonByName("a");

//            future.get()
//            long nanos = TimeUnit.SECONDS.toNanos(3);
//            ArrayBlockingQueue queue = new ArrayBlockingQueue(1);
//            long currentTimeMillis = System.currentTimeMillis();
//            ConcurrentLinkedQueue queue1 = new ConcurrentLinkedQueue();

        }
        long time = System.currentTimeMillis() - start;
        System.out.println("loop: " + loopCount);
        System.out.println("time: " + time + "ms");
        System.out.println("tps: " + loopCount * 1000 / time);
        System.exit(0);


    }
}
