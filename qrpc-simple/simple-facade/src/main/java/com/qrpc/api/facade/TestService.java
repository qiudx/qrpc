package com.qrpc.api.facade;

import com.qrpc.api.vo.User;
import com.qrpc.common.annotation.Qasync;
import com.qrpc.api.vo.Person;


public interface TestService {
    String getString();

    Person getPerson(String name);

    @Qasync
    Object getPersonByName(String name);

    User getUser(String userId);
}
