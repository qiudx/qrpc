/**
 * Zentech-Inc
 * Copyright (C) 2017 All Rights Reserved.
 */
package com.qrpc.api.facade;

/**
 * @author qiudx
 * @version $Id TestService2.java, v 0.1 2017-09-11 17:39 qiudx Exp $$
 */
public interface TestService2 {
    int add(int a, int b);
}

