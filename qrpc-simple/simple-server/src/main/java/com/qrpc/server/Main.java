package com.qrpc.server;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:qrpc-context.xml");
        context.registerShutdownHook();
        context.start();
    }
}
