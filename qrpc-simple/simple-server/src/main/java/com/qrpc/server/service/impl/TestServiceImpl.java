package com.qrpc.server.service.impl;

import com.qrpc.api.facade.TestService;
import com.qrpc.api.vo.Person;
import com.qrpc.api.vo.User;
import org.springframework.stereotype.Service;

@Service("com.qrpc.api.facade.TestService")
public class TestServiceImpl implements TestService {
    @Override
    public String getString() {
        return "爱生活,爱Java";
    }

    @Override
    public Person getPerson(String name) {
        Person person = new Person();
        person.setName(name);
        person.setAge(20);
        return person;
    }

    @Override
    public Person getPersonByName(String name) {
        Person person = new Person();
        person.setName(name);
        person.setAge(50);
        return person;
    }

    @Override
    public User getUser(String userId) {
        User user = new User();
        user.setUserId(userId);
        user.setUserName("qiudx");
        return user;
    }
}
