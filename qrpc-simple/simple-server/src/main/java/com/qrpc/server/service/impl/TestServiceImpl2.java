package com.qrpc.server.service.impl;

import com.qrpc.api.facade.TestService2;
import org.springframework.stereotype.Service;

@Service("com.qrpc.api.facade.TestService2")
public class TestServiceImpl2 implements TestService2 {
    @Override
    public int add(int a, int b) {
        return 0;
    }
}
